# Теория и практика concurrency

_"You don't have to be an engineer to be a racing driver, but you do have to have Mechanical Sympathy"_ – Jackie Stewart, racing driver

Добро пожаловать в репозиторий курса!

- Прочтите [Getting Started](/docs/getting-started.md) прежде чем клонировать этот репозиторий.

- [Как сдавать задачи](/docs/ci.md)
- [Приложение](http://tpcc.btty.su:5222/)
- [Scoreboard](https://docs.google.com/spreadsheets/d/1dWPtvH6DStrHuJO7EMlqobdnNItZFK5qtyYQgDxEQAw/edit?usp=sharing)

## Навигация

* `docs` – инструкции, стайлгайд

* `tasks` – домашние задания

* `lectures` – план курса и расписание лекций, ссылки

* `library` – библиотека для тестирования решений

* `client` – консольный клиент для работы с задачами

Остальные директории – служебные.
