#include "queue_spinlock.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/strand/test.hpp>

#include <thread>

using QSpinLock = solutions::QueueSpinLock;

TEST_SUITE(QueueSpinLock) {
  SIMPLE_T_TEST(LockUnlock) {
    QSpinLock spinlock;

    {
      QSpinLock::Guard guard(spinlock);  // acquired
      // critical section
    }  // released
  }

  SIMPLE_T_TEST(SequentialLockUnlock) {
    QSpinLock spinlock;

    {
      QSpinLock::Guard guard(spinlock);
    }
    {
      QSpinLock::Guard guard(spinlock);
    }
  }

  SIMPLE_T_TEST(ConcurrentIncrements) {
    QSpinLock spinlock;
    size_t shared_counter = 0;

    const size_t kIncrementsPerThread = 1000;

    auto routine = [&]() {
      for (size_t i = 0; i < kIncrementsPerThread; ++i) {
        QSpinLock::Guard guard(spinlock);

        size_t current = shared_counter;
        twist::strand::this_thread::sleep_for(
            std::chrono::microseconds(10));
        shared_counter = current + 1;
      }
    };

    twist::strand::thread t1(routine);
    twist::strand::thread t2(routine);
    t1.join();
    t2.join();

    ASSERT_EQ(shared_counter, 2 * kIncrementsPerThread);
  }
}

RUN_ALL_TESTS()
